---
title: 'Introducing BeBlob: A GitLab-Powered Comment Section for Static Websites'
date: 2025-02-20 21:56:46
tags:
    - beblob
    - gitlab
    - opensource
---

I'm excited to announce the launch of [BeBlob](https://gitlab.com/antonbelev/beblob) – an open-source widget that brings a full-fledged comment section to your static or dynamic website, powered entirely by GitLab!

## What is BeBlob?

BeBlob is a lightweight commenting system that uses GitLab issues as its backend. With BeBlob, each page on your site is automatically mapped to a GitLab issue where visitors can leave comments, reply to discussions, and even react with emojis. By leveraging GitLab OAuth for user authentication, the system ensures that every comment is properly attributed.

## Why I Built BeBlob

After launching my blog, I set a goal for myself to expand its features with one particular constraint — don’t pay for services apart from the domain name.  

This is an interesting constraint when you start thinking about how many features require a backend or database. A comment section was one of the first features I considered, and I quickly found projects like [utterances](https://github.com/utterance/utterances) and [giscus](https://laymonage.com/posts/giscus), which use GitHub Issues and Discussions to provide this functionality. Since I chose to host my personal blog using GitLab Pages, I could have used these services by maintaining a separate repository on GitHub just for comments. However, I wanted a native GitLab solution, so I decided to build the same feature — but for GitLab. And that’s when BeBlob was born.

## Key Features

- **GitLab OAuth Integration:**  
  Users authenticate via their GitLab account, ensuring comments are tied to a verified identity.

- **Issue-Based Commenting:**  
  Each page maps to a GitLab issue where comments and discussions are stored, taking advantage of GitLab's native features.

- **Emoji Reactions:**  
  Visitors can quickly express their feedback with predefined emoji reactions.

- **Markdown Support with Live Preview:**  
  Write and preview comments in Markdown for a rich commenting experience. Including code-highlighting.

- **Customizable Themes:**  
  Inspired by my use of the Hexo Cactus theme on my blog, BeBlob offers four themes (dark, white, light, and classic) to ensure the comment section integrates perfectly with your site design.

## Demo and Source Code

I've already added BeBlob to my personal blog (just scroll below), so I'm the first real user! You can check out the demo to see all available themes:

- [https://antonbelev.gitlab.io/beblob-demo/](https://antonbelev.gitlab.io/beblob-demo/)
  
The complete source code is available on GitLab:

- [https://gitlab.com/antonbelev/beblob](https://gitlab.com/antonbelev/beblob)


## Looking Ahead

BeBlob is still a work in progress and far from being polished. My goal is to get it out there early so I can receive feedback from real users and iterate on the design and functionality. If you try it out and have suggestions, bug reports, or ideas for new features, please feel free to open an issue or reach out.

I’m excited to see how BeBlob can help other GitLab users add interactive comment sections to their sites.

---

*Feel free to leave your thoughts in the comments below or get in touch directly.*
