---
title: Improving your Merge Requests using AI and Mermaid Diagrams
date: 2025-02-03 18:27:29
tags:
  - ai
  - merge-requests
  - mermaid
---

Engineers, have you ever wished you could automatically create detailed diagrams of your code without the manual hassle? With AI, it's now possible to generate **[Mermaid.js](https://github.com/mermaid-js/mermaid)** diagrams directly from your code implementations—making documentation and merge requests (MRs) clearer and more effective.

## Why Use AI-Generated Mermaid Diagrams?

1. **Version-Controlled Documentation:**  
   Since Mermaid diagrams are defined in Markdown, they can be stored alongside your source code. This ensures your diagrams stay updated and versioned with your project.

2. **Seamless Integration in Merge Requests:**  
   Embedding diagrams directly into merge requests means they render automatically in platforms like GitHub and GitLab. This provides immediate visual context for code changes.

3. **Visual Insights from Your Code:**  
   Perhaps the biggest advantage is the ability to feed your code to an AI tool and receive a detailed sequence diagram or flowchart in return. This visual output can help quickly explain complex interactions within your code.

### Example: Generating a Sequence Diagram from Java Code

Consider the following Java implementation of a simple **order processing service**:

```java
public class OrderService {
    private PaymentService paymentService;
    private InventoryService inventoryService;

    public OrderService(PaymentService paymentService, InventoryService inventoryService) {
        this.paymentService = paymentService;
        this.inventoryService = inventoryService;
    }

    public boolean processOrder(String orderId) {
        if (!inventoryService.reserveItem(orderId)) {
            return false;
        }
        boolean paymentSuccess = paymentService.processPayment(orderId);
        if (!paymentSuccess) {
            inventoryService.releaseItem(orderId);
            return false;
        }
        return true;
    }
}
```

### Example AI Prompt

You can feed this Java code into your favourite LLM tool with a prompt like:

> Analyze the OrderService and generate a Mermaid.js sequence diagram representing the order processing flow. Include interactions between `OrderService`, `PaymentService`, and `InventoryService`.

### Example AI-Generated Mermaid Diagram

```
sequenceDiagram
participant User
participant OrderService
participant InventoryService
participant PaymentService

    User->>OrderService: processOrder(orderId)
    OrderService->>InventoryService: reserveItem(orderId)
    InventoryService-->>OrderService: success/fail
    
    alt If reservation fails
        OrderService-->>User: return false
    else If reservation succeeds
        OrderService->>PaymentService: processPayment(orderId)
        PaymentService-->>OrderService: success/fail
        alt If payment fails
            OrderService->>InventoryService: releaseItem(orderId)
            OrderService-->>User: return false
        else If payment succeeds
            OrderService-->>User: return true
        end
    end
```

And here is how this diagram renders in IntelliJ, GitLab, and GitHub merge request views.  
If you use IntelliJ, you can install the [Mermaid plugin](https://plugins.jetbrains.com/plugin/20146-mermaid) to preview diagrams directly within the IDE.

```mermaid
sequenceDiagram
participant User
participant OrderService
participant InventoryService
participant PaymentService

    User->>OrderService: processOrder(orderId)
    OrderService->>InventoryService: reserveItem(orderId)
    InventoryService-->>OrderService: success/fail
    
    alt If reservation fails
        OrderService-->>User: return false
    else If reservation succeeds
        OrderService->>PaymentService: processPayment(orderId)
        PaymentService-->>OrderService: success/fail
        alt If payment fails
            OrderService->>InventoryService: releaseItem(orderId)
            OrderService-->>User: return false
        else If payment succeeds
            OrderService-->>User: return true
        end
    end
```

### Benefits of Using AI for Mermaid Diagrams

- **Automatic Documentation**: The generated diagrams can be stored in source control alongside the code, ensuring they remain up to date.
- **Better Code Reviews**: Reviewing a complex MR becomes easier when accompanied by a sequence diagram or flowchart.
- **MR-Friendly Format**: Since Mermaid diagrams can be embedded in Markdown files (`.md`), they can be rendered directly in MR descriptions.
- **AI-Powered Insights**: AI can quickly extract logical flows from the code and visualize them, saving engineers time and reducing errors in manual documentation.

### Final Thoughts

Encouraging the use of AI-generated Mermaid diagrams in MRs can vastly improve collaboration, clarity, and maintainability. The next time you work on a complex MR, try feeding your code into an AI tool and let it generate a helpful diagram—it might just make your reviewers’ lives a whole lot easier!
