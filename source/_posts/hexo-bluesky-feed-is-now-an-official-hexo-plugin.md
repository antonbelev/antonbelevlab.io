---
title: hexo-bluesky-feed is now an official Hexo plugin
date: 2025-02-13 15:10:23
tags:
    - hexo
    - hexo-bluesky-feed
    - opensource
    - bluesky
---

I’m thrilled to announce that `hexo-bluesky-feed` is now an official [Hexo plugin](https://hexo.io/plugins/)!

![hexo-bluesky-feed now an official hexo plugin](../images/blog-posts/hexo-bluesky-feed.png)

A huge thanks to the **Hexo team** for their swift review and approval!

Meanwhile, I’m already working on a new Hexo plugin — bigger and more impactful — so stay tuned! 🚀

