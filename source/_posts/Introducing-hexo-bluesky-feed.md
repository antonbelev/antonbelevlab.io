---
title: Introducing hexo-bluesky-feed
date: 2025-02-10 22:44:00
tags:
  - hexo
  - bluesky
  - opensource
  - npm
  - hexo-bluesky-feed
---

I'm excited to announce **hexo-bluesky-feed** — an open source Hexo plugin that can publish your latest blog post to [Bluesky](https://bsky.app) whenever you generate your Hexo site **with the `--bluesky` flag**.

With **hexo-bluesky-feed**, if you run `hexo generate --bluesky`, the plugin will:

- **Obtain a fresh access token:** It uses your Bluesky handle and an app password (set via environment variables) to securely fetch a new access token and your DID.
- **Select your latest post:** The plugin sorts your posts by their publication date, ensuring the most recent post is used.
- **Construct a dynamic update message:** It builds a message using a customizable template and attaches a clickable link to your post.
- **Post directly to Bluesky:** The update is sent using Bluesky's API so that your social feed stays up-to-date automatically.

> **Note**: If you run `hexo generate` _without_ the `--bluesky` flag, the plugin will skip posting.

### Where to Find hexo-bluesky-feed

- **npm Package:**  
  Check out the plugin on npm: [hexo-bluesky-feed](https://www.npmjs.com/package/hexo-bluesky-feed)

- **Source Code:**  
  The project is open source and available on GitHub: [antonbelev/hexo-bluesky-feed](https://github.com/antonbelev/hexo-bluesky-feed)

- **My Bluesky Profile:**  
  Follow my updates on Bluesky: [antonbelev.bsky.social](https://bsky.app/profile/antonbelev.bsky.social)

### Why Use hexo-bluesky-feed?

This tool is designed to streamline your blogging workflow:

- **Controlled Social Sharing:** Decide exactly when to post by adding `--bluesky` to your generation command.
- **Effortless Integration:** Simply set your environment variables in a `.env` file (dotenv is bundled) and configure your post link and message in your Hexo `_config.yml`.
- **Open Source and Extensible:** With all source code on GitHub, you can contribute, customize, or simply learn from it.

Give it a try and let me know what you think! Your feedback and contributions are always welcome.

Happy blogging—and happy posting!
