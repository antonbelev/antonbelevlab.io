---
title: How to build your first lightweight blog using Hexo and Cactus
date: 2024-03-06 18:18:18
tags:
  - hexo
  - blog
---

## Why Hexo?

For some time I wanted to upgrade my very basic personal website hosted on GitHub Pages - [https://antonbelev.github.io/](https://antonbelev.github.io/).

I had the following requirements: 

- I wanted to open an IDE and just write down plain markdown.
- Be able to push new version of my site directly from my terminal.
- Be able to write posts, link my contact details and any side-projects I ever build.
- Be able to highlight code syntax (I guess that's a given if you are using markdown).
- Have a clean, plain look and feel, allowing users to easily navigate through posts. I wanted the simplicity of Hacker News.
- Extra points if it supports tags/search/comments.

[Hexo](https://hexo.io/) hit most of these requirements. It supports a number of themes, which allows you to pick the right look and feel for you. This is where [Cactus](https://github.com/probberechts/hexo-theme-cactus) comes into the picture. I loved the design immediately.


## Hosting

I didn't want to spend any money on hosting, so for me the clear choice was between [GitHub Pages](https://pages.github.com/) and [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) - both allowing you to host your static website for free.

I decided to go with GitLab Pages, simply because I'm already using my GitHub Pages for my old website.


## Deploy your Hexo blog on GitLab Pages

In order to deploy your blog on GitLab Pages you can follow this guide by Hexo - https://hexo.io/docs/gitlab-pages.html.

My `.gitlab-ci.yml` is slightly different [view it here](https://gitlab.com/antonbelev/antonbelev.gitlab.io/-/blob/main/.gitlab-ci.yml?ref_type=heads).

```yml
image: node:16-alpine

cache:
  paths:
    - node_modules/

before_script:
  - npm install hexo-cli -g
  - npm install

job:
  script: echo "hexo blog build"
  artifacts:
    paths:
      - public

pages:
  script:
    - hexo generate
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

The `job` section allows me to download the generated artifacts. I had an issue where the GitLab Pages was loading empty page. This allowed me to notice that the `index.html` was empty. More about that in the next section!


## Git wasn't tracking the clone `themes/cactus` directory

The very first step from the [hexo-theme-cactus installation guide](https://github.com/probberechts/hexo-theme-cactus?tab=readme-ov-file#install) asks you to clone the theme into the `themes/cactus` directory. I didn't initially notice but this my top-level hexo project was a github repo, cloning another repo in a sub-folder caused the `git add .` command to ignore that directory. I think that's related to git submodules - see more [here](https://stackoverflow.com/questions/53902804/git-is-not-tracking-hexo-theme-file). As a result Git wasn't tracking the theme folder.
This was causing the `index.html` to be empty in my artifact, thus the blog appeared blank.

### Solution
1. Remove the untracked theme folder:
Since Git is not tracking the `themes/cactus` folder properly, we should remove it and then re-add it.

```bash
$ git rm -r --cached themes/cactus
```
This command will remove the `themes/cactus` directory from the Git index (the staging area), but it will not delete the actual files from your working directory.

2. Re-add the theme folder:

```bash
$ git add themes/cactus
```

Now `themes/cactus` should be properly tracked by Git.

## Getting Domain name

In the past I used GoDaddy. This time I wanted to show around. For 2 years plan for `belev.me`, GoDaddy was quoting almost £50, including privacy setting, which is basically masking my contact details from `whois`, to avoid spam.

I found `namecheap` which was ranking quite high when I searched for a place to buy a domain name from. The quoted me about £37 for 3 years with Free Domain Privacy. Great.

## GitLab Pages with custom domain name

When you setup GitLab Pages you get a public URL which is you `username.gitlab.io`. However you have the option to specify your own domain. [Here](https://www.namecheap.com/support/knowledgebase/article.aspx/10446/2208/how-do-i-link-my-domain-to-gitlab-pages/) is a great guide I followed from `namecheap`. On GitLab's side you can follow [this guide](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/).

One thing to watch out for is when you go to your GitLab repository -> Deploy -> Pages -> New Domain. GitLab will give you a `Verification status`. This is a `TXT` record you need to put on `namecheap`. The TXT record should be for `_gitlab-pages-verification-code` as per the `namecheap` guide and not `_gitlab-pages-verification-code.belev.me` as suggested by GitLab.

### You don't need to buy SSL cert

Another expense could've been the SSL cert. Luckily GitLab is using [Let's Encrypt](https://letsencrypt.org/). Let's Encrypt is a free, automated, and open certificate authority (CA) that gives digital certificates in order to enable HTTPS (SSL/TLS) for websites.

Note - after you've successfully setup your `A`, `CNAME` and `TXT` records, it takes awhile for GitLab and Let's Encrypt to generate your SSL cert. So initially you will see your website resolving with a warning in Chrome (or your browser) saying that your website is `Not Secure`. For me it took 15-30min for this to propagate.

--- 

Hopefully, this high-level guide will get you started ✌️!
Thanks for reading!