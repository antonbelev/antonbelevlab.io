---
title: about
date: 2024-03-05 12:05:51
---

## About me
![Anton Belev](about/a/me.jpg "That's me")


Hi 👋

My name is Anton Belev, and I'm a Software Engineer with more than 9 years of experience. I'm from Sofia, Bulgaria, however I'm currently based in Glasgow, Scotland. Most recently I've enjoyed my very first start-up experience as a Founding Engineer at Prehendo, a retail media B2B SaaS company. 

Before Prehendo, I gained a lot of experience on engineering excellence and best-practices by working in a pre-IPO and scale-ups. I worked as a backend-engineer at Deliveroo and I even played engineering lead role for a couple of projects. My highlight include scaling the Rewards feature tenfold, from having it only available to Plus users to the entire customer base in multiple markets (from 1M Plus subscribers to 10M users at the time). Before that I worked at Skyscanner, Morgan Stanley and The Hut Group (also known as THG, some of the more recognised brands owned by the company include Myprotein, Lookfantastic and others).


### Why building this website?

I've always wanted to create a personal space which looks simple and allows me to share my ideas and thoughts as well as showing my side-projects which right now are not many.
This is when I found [hexo](https://hexo.io/) and specifically the [cactus theme](https://github.com/probberechts/hexo-theme-cactus) which I used to build this blog.

### Contact me
Best way to reach out is by messaging me on [LinkedIn](https://www.linkedin.com/in/anton-belev/).
